TextFormField(
  controller: textController4,
  onChanged: (_) => EasyDebounce.debounce(
    'textController4',
    Duration(milliseconds: 2000),
    () => setState(() {}),
  ),
  autofocus: true,
  obscureText: false,
  decoration: InputDecoration(
    labelText: 'password',
    hintText: '[Some hint text...]',
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(
        color: Color(0x00000000),
        width: 1,
      ),
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(4.0),
        topRight: Radius.circular(4.0),
      ),
    ),
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(
        color: Color(0x00000000),
        width: 1,
      ),
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(4.0),
        topRight: Radius.circular(4.0),
      ),
    ),
  ),
  style: FlutterFlowTheme.of(context).bodyText1,
)

